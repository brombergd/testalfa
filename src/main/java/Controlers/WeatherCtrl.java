package Controlers;

import Model.ResponseModel.Response;
import ModelRequest.ModelRequest;
import Utils.PropertyManager;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import static io.restassured.RestAssured.given;
import  static io.restassured.RestAssured.requestSpecification;


public class WeatherCtrl {

    public WeatherCtrl(ModelRequest modelRequest) {

         String url  = PropertyManager.getInstance().getURL();
         String basePath = PropertyManager.getInstance().getBasePath();
         String apiKey = PropertyManager.getInstance().getApiKey();
         String apiKeyValue  = PropertyManager.getInstance().getApiKeyValue();

        if(requestSpecification == null) {
            requestSpecification = new RequestSpecBuilder()
                        .addHeader(apiKey, apiKeyValue)
                        .setBaseUri(url)
                        .setBasePath(basePath)
                        .addQueryParam("lat", modelRequest.getValLat())
                        .addQueryParam("lon", modelRequest.getValLon())
                        .addQueryParam("limit", modelRequest.getLimit())
                        .log(LogDetail.ALL)
                        .build();
            }
    }

    public Response getWeather(){
        return given()
                 .when()
                 .get()
                 .then()
                 .extract()
                 .as(Response.class);
    }

}
