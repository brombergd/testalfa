package Model.ResponseModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.processing.Generated;

@Generated("com.robohorse.robopojogenerator")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tzinfo{

	@JsonProperty("offset")
	private int offset;

	@JsonProperty("dst")
	private boolean dst;

	@JsonProperty("name")
	private String name;

	@JsonProperty("abbr")
	private String abbr;

	public int getOffset(){
		return offset;
	}

	public boolean isDst(){
		return dst;
	}


	public String getName(){
		return name;
	}

	public String getAbbr(){
		return abbr;
	}

}