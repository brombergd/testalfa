package Model.ResponseModel;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.processing.Generated;

@Generated("com.robohorse.robopojogenerator")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Info{

	@JsonProperty("lon")
	private double lon;

	@JsonProperty("tzinfo")
	private Tzinfo tzinfo;
	@JsonProperty("url")

	private String url;

	@JsonProperty("lat")
	private double lat;

	public double getLon(){
		return lon;
	}

	public Tzinfo getTzinfo(){
		return tzinfo;
	}

	public String getUrl(){
		return url;
	}

	public double getLat(){
		return lat;
	}


}