package Model.ResponseModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.processing.Generated;

@JsonIgnoreProperties(ignoreUnknown = true)
@Generated("com.robohorse.robopojogenerator")
public class ForecastsItem{

	@JsonProperty("moon_text")
	private String moonText;

	public String getMoonText(){
		return moonText;
	}




}