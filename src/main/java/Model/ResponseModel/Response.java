package Model.ResponseModel;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.processing.Generated;

@Generated("com.robohorse.robopojogenerator")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response{

	@JsonProperty("now_dt")
	private String nowDt;

	@JsonProperty("fact")
	private Fact fact;

	@JsonProperty("now")
	private int now;

	@JsonProperty("info")
	private Info info;

	@JsonProperty("forecasts")
	private List<ForecastsItem> forecasts;

	public Fact getFact(){
		return fact;
	}

	public Info getInfo(){
		return info;
	}

	public  List<ForecastsItem> getForecasts(){
		return forecasts;
	}


}