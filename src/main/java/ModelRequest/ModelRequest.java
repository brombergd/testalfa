package ModelRequest;

import java.util.HashMap;
import java.util.Map;

public class ModelRequest {

    private double lat;
    private double lon;
    private int    offset;
    private String name;
    private int limit;


    public void setValueLat(double lat){
        this.lat = lat;
    }
    public double getValLat(){
        return lat;
    }
    public void setValueLon(double lon){
        this.lon = lon;
    }
    public double getValLon(){
        return lon;
    }
    public void setOffset(int offset){
        this.offset = offset;
    }
    public int getOffset(){
        return offset;
    }
    public void setLimit(int limit){
        this.limit = limit;
    }
    public int getLimit(){
        return limit;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }


}
