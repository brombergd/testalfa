package Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {

    private static PropertyManager instance;
    private static final Object lock = new Object();
    private static String propertyFilePath = System.getProperty("user.dir")+
            "\\src\\main\\resources\\configuration.properties";
    private static String url;
    private static String basePath;
    private static String apiKey;
    private static String apiKeyValue;
    private static String pageUrl;

    public static PropertyManager getInstance ()  {
        if (instance == null) {
                instance = new PropertyManager();
                instance.loadData();
        }
        return instance;
    }
    private void loadData() {

        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(propertyFilePath));
        } catch (IOException e) {
            System.out.println("Configuration properties file cannot be found");
        }

        url = prop.getProperty("url");
        basePath = prop.getProperty("basePath");
        apiKey = prop.getProperty("apiKey");
        apiKeyValue = prop.getProperty("apiKeyValue");
        pageUrl = prop.getProperty("pageUrl");


    }
    public String getURL () {
        return url;
    }
    public String getBasePath(){
        return basePath;
    }
    public String getApiKey(){
        return apiKey;
    }
    public String getApiKeyValue(){
        return apiKeyValue;
    }
    public String getPageUrl(){
        return pageUrl;
    }
}
