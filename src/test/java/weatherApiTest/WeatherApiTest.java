package weatherApiTest;

import Controlers.WeatherCtrl;
import Model.ResponseModel.Response;
import ModelRequest.ModelRequest;
import Utils.PropertyManager;
import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;


public class WeatherApiTest {

    @Test
    public void compareResponse()  {
        //arrange

        ModelRequest param = new ModelRequest();
        param.setValueLat(55.75396);
        param.setValueLon(37.620393);
        param.setLimit(2);
        param.setOffset(10800);
        param.setName("Europe/Moscow");
        //act
        WeatherCtrl modelCtrl=  new WeatherCtrl(param);
        Response response = modelCtrl.getWeather();

        //assert
        SoftAssertions apiAssertions = new SoftAssertions();

        apiAssertions.assertThat(response.getInfo().getLat())
                     .as("lat должна равняться ожидаемому")
                     .isEqualTo(param.getValLat());
        apiAssertions.assertThat(response.getInfo().getLon())
                     .as("Lon должна равняться ожидаемому")
                     .isEqualTo(param.getValLon());
        apiAssertions.assertThat(response.getInfo().getTzinfo().getOffset())
                     .as("Offset должен равняться ожидаемому")
                     .isEqualTo(param.getOffset());
        apiAssertions.assertThat(response.getInfo().getTzinfo().getName())
                     .as("Tzinfo должно равняться ожидаемому")
                     .isEqualTo(param.getName());
        apiAssertions.assertThat(response.getInfo().getTzinfo().getAbbr())
                     .as("abbr должнен равняться ожидаемому")
                     .isEqualTo("MSK");
        apiAssertions.assertThat(response.getInfo().getUrl())
                     .as("url на странице должен равняться ожидаемому ожидаемому")
                     .isEqualTo(PropertyManager.getInstance().getPageUrl());
        apiAssertions.assertThat(response.getFact().getSeason())
                     .as("season должен равняться ожидаемому")
                     .isEqualTo("autumn");
        apiAssertions.assertThat(response.getForecasts().size())
                     .as("размер массива должен равняться ожидаемому")
                     .isEqualTo(2);
        apiAssertions.assertThat(response.getInfo().getTzinfo().isDst())
                     .as("dst должен равняться ожидаемому")
                     .isEqualTo(false);
        apiAssertions.assertAll();

    }

}
